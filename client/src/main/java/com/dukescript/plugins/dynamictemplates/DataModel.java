package com.dukescript.plugins.dynamictemplates;

import com.dukescript.plugins.dynamictemplates.js.TemplateRegistration;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.FileChooser;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.Models;
import net.java.html.json.Property;

@Model(className = "Data", targetId = "", properties = {
    @Property(name = "template", type = String.class),
    @Property(name = "contentA", type = String.class),
    @Property(name = "contentB", type = String.class),   
    @Property(name = "errorMessage", type = String.class), 
    @Property(name = "theaterReviews", type = TheaterReview.class, array = true),
    @Property(name = "editedTheaterReview", type = TheaterReview.class),
    @Property(name = "synesthesiaValues", type = String.class, array = true),
    @Property(name = "ratingValues", type = int.class, array = true),
    @Property(name = "directories", type = String.class, array = true),
    @Property(name = "directoryValues", type = String.class, array = true),
    @Property(name = "editedfiletitle", type= String.class),
    @Property(name = "editeduploadedfile", type= String.class),
    @Property(name = "uplfiles", type = UplFile.class, array=true),
    @Property(name = "currentMenu", type = int.class),
    
})

final class DataModel {

    private static Data ui;
    private static Closeable a;
    private static Closeable b;
    private static Closeable f;
    
    
    @Model(className="UplFile", properties = {
        @Property(name = "origFileName", type = String.class),
        @Property(name = "targetFileName", type = String.class),
        @Property(name = "userName4File", type = String.class),
    })
    
    public static class UplFileVMD{
        
    }
    
    
    
    @Function
    public static void uploadFile(Data model){
        
        String dasFile = showFileDialog("Neue Datei hinzufügen", ".txt .doc .xls .pdf .xml");
        //TODO weitere Prüfungen: Datei isFile usw.
        if (dasFile.length()>0) {
                File file = new File(dasFile);
        model.setEditeduploadedfile(file.getAbsolutePath());
        System.out.println("Editierter Titel des Nutzers: "+model.getEditedfiletitle());
        System.out.println("Gewählte Datei ist: "+file.getAbsolutePath());
        
        }
        else{
           System.out.println("Keine Datei ausgewählt!");  
        }

    }
    
    
     public static String showFileDialog(String strTitel, String strAllowedFileExts) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(strTitel);
        String[] split = strAllowedFileExts.split(" ");
        for (int i = 0; i < split.length; i++) {
            split[i] = "*" + split[i];
            System.out.println("Erlaubte Endungen:  " + split[i]);
        }

        FileChooser.ExtensionFilter extFilter
                = new FileChooser.ExtensionFilter(strAllowedFileExts, split);
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            return file.getAbsolutePath();
        } else {
            return null;
        }
    }    
    
    
    

    public static void init() {
        
        ui = new Data();
        Models.toRaw(ui);
        a = TemplateRegistration.register("a", "a.html");
        b = TemplateRegistration.register("b", "b.html");
        f = TemplateRegistration.register("f", "file.html");
        
        ui.setTemplate("a");
        ui.setContentA("This is Content for Template A!");
        ui.setContentB("This is Content for Template B!");
        
        ui.getRatingValues().add(1);
        ui.getRatingValues().add(2);
        ui.getRatingValues().add(3);
        ui.getRatingValues().add(4);
        ui.getRatingValues().add(5);
        
        ui.getSynesthesiaValues().add("rot");
        ui.getSynesthesiaValues().add("grün");
        ui.getSynesthesiaValues().add("blau");
        ui.getSynesthesiaValues().add("orange");
        ui.getSynesthesiaValues().add("gelb");
        
        ui.getDirectoryValues().add("Ziel-Verzeichnis A 1");       
        ui.getDirectoryValues().add("Ziel-Verzeichnis A 2");
        ui.getDirectoryValues().add("Ziel-Verzeichnis B 1");
        ui.getDirectoryValues().add("Ziel-Verzeichnis B 2");
    }
    
    /**
     * Called when the page is ready.
     */
    static void onPageLoad() throws Exception {
        init();
        ui.applyBindings();
    }

    @Model(className = "TheaterReview", properties = {
        @Property(name = "performance", type = String.class), 
        @Property(name = "dateOfVisit", type = String.class), 
        @Property(name = "content", type = String.class),   
        @Property(name = "synesthesia", type = String.class, array = true),
        @Property(name = "rating", type = int.class),
    })
    
    public static class TheaterReviewVMD {
        
    }
    


    @Function
    public static void setA(Data model) {
        model.setTemplate("a");
        model.setCurrentMenu(1);
    }

    @Function
    public static void setB(Data model) {
        model.setTemplate("b");
        model.setCurrentMenu(2);
    }
    
    @Function
    public static void setF(Data model) {
        model.setTemplate("f");
        model.setCurrentMenu(3);
    }
    
    @Function
    public static void savePerformanceRatingSetB(Data model) {

        String strDate = model.getEditedTheaterReview().getDateOfVisit();

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        try {

            if (strDate == null) {
                model.setErrorMessage("Bitte geben Sie ein gültiges Datum an: dd.mm.yyyy");
                model.setTemplate("a");
            } else {
                Date test = sdf.parse(strDate);
                model.getTheaterReviews().add(model.getEditedTheaterReview());
                System.out.println("Datum OK: " + strDate);
                model.setEditedTheaterReview(new TheaterReview());

                model.setTemplate("b");
                model.setContentB("Bewertung der Vorstellung erfolgreich gespeichert.");
            }
        } catch (ParseException pe) {
            System.out.println("Datum nicht gültig! strDate" + strDate);
            model.setErrorMessage("Bitte geben Sie ein gültiges Datumsformat an: dd.mm.yyyy");
            model.setTemplate("a");
        }

    }

}
